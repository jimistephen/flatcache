//
// Created by Stephen Vickers on 10/29/18.
// Copyright (c) 2018 Stephen Vickers. All rights reserved.
//

import Foundation

public struct FlatCacheKey: Equatable, Hashable{
    let typeName: String
    let id: String
}

public protocol Identifiable{
    var id: String {get}
}

public protocol Cachable: Identifiable{}

private extension Cachable{
    static var typeName: String{
        return String(describing: self)
    }

    var flatCacheKey: FlatCacheKey{
        return FlatCacheKey(typeName: Self.typeName, id: id)
    }
}

public protocol FlatCacheListener: AnyObject{
    func flatCacheDidUpdate(cache: FlatCache, update: FlatCache.Update)
}

public final class FlatCache {

    public enum Update{
        case item(Any)
        case list([Any])
        case clear
    }

    private var storage: [FlatCacheKey: Any] = [:]

    private let queue = DispatchQueue(label: "come.freetime.FlatCache.queue", qos: .userInitiated, attributes: .concurrent)

    private var listeners: [FlatCacheKey: NSHashTable<AnyObject>] = [:]

    public init(){

    }

    public func add<T: Cachable>(listener: FlatCacheListener, value: T){
        assert(Thread.isMainThread)

        let key = value.flatCacheKey
        let table: NSHashTable<AnyObject>

        if let existing = self.listeners[key]{
            table = existing
        }else{
            table = NSHashTable.weakObjects()
        }

        table.add(listener)
        self.listeners[key] = table


    }

    public func set<T: Cachable>(value: T){
        assert(Thread.isMainThread)

        let key = value.flatCacheKey
        self.storage[key] = value

        self.enumerateListeners(key: key) { listener in
            listener.flatCacheDidUpdate(cache: self, update: .item(value))
        }
    }

    public func set<T: Cachable>(values: [T]){
        assert(Thread.isMainThread)

        var listenerHashToValueMap = [Int: [T]]()
        var listenerHashToListenerMap = [Int: FlatCacheListener]()

        for value in values{
            let key = value.flatCacheKey
            self.storage[key] = value

            self.enumerateListeners(key: key, block: { listener in
                let hash = ObjectIdentifier(listener).hashValue
                if var arr = listenerHashToValueMap[hash] {
                    arr.append(value)
                    listenerHashToValueMap[hash] = arr
                }else{
                    listenerHashToValueMap[hash] = [value]
                }
                listenerHashToListenerMap[hash] = listener
            })
        }

        for (hash, arr) in listenerHashToValueMap{
            guard let listener = listenerHashToListenerMap[hash] else {continue}
            if arr.count == 1, let first = arr.first{
                listener.flatCacheDidUpdate(cache: self, update: .item(first))
            }else{
                listener.flatCacheDidUpdate(cache: self, update: .list(arr))
            }
        }
    }

    public func get<T: Cachable>(id: String) -> T? {
        assert(Thread.isMainThread)

        let key = FlatCacheKey(typeName: T.typeName, id: id)
        return self.storage[key] as? T
    }

    public func clear(){
        assert(Thread.isMainThread)

        self.storage = [:]

        for key in self.listeners.keys{
            self.enumerateListeners(key: key){ listener in
                listener.flatCacheDidUpdate(cache: self, update: .clear)
            }
        }
    }

    //MARK: - Private Functions -

    private func enumerateListeners(key: FlatCacheKey, block: (FlatCacheListener) ->()){
        assert(Thread.isMainThread)

        if let table = self.listeners[key]{
            for object in table.objectEnumerator(){
                if let listener = object as? FlatCacheListener{
                    block(listener)
                }
            }
        }
    }

}